FROM gimbalabs/footloose-ubuntu:20.04

RUN echo root:CHANGEME | chpasswd
ENV APT_ARGS="-y -o APT::Install-Suggests=false -o APT::Install-Recommends=false"
RUN apt update -qq && \
    apt install ${APT_ARGS} docker.io iptables linux-image-generic initramfs-tools && \
    update-initramfs -c -k all && \
    systemctl enable docker systemd-networkd

COPY assets/etc/systemd/network /etc/systemd/network
