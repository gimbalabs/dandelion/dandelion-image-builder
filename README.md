# README

Executing the build script [0] will create 3 images:

- dandelion-worker-x86_64.img: raw disk image. You can `dd` this to your physical disk, but it contains a 2gb only partition that you will need to resize accordingly to your disk size.
- dandelion-worker-x86_64.qcow2: qcow2 image containing a 200gb virtual disk. You can `dd` it directly to your 200gb+ disk with:
```
qemu-img dd -f qcow2 -O raw if=dandelion-worker-x86_64.qcow2 of=/dev/_CHANGEME_
```
- dandelion-worker-x86_64.vdi: You can use this one to boot from VirtualBox. Note that dandelion for testnet AND maninnet runs here, so you'll need ~32gb of RAM on your virtual machine :)

[0]
`bash -x build-images.sh`

# Build base image

This is the docker image that the disk images will use as base.

```
docker build -t gimbalabs/dandelion-footloose:ubuntu20.04 .
```

# Build builder image

This is an auxiliary image containing tools needed to create the image.

```
docker build -f Dockerfile.builder -t gimbalabs/dandelion-image-builder:builder .
```

TODO: do everything from within this image instead of relying on developer's environment for footloose, git, kubectl, helm, kustomize...
