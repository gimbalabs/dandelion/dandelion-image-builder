#!/bin/bash

set -e

echo_blue() {
    local font_blue="\033[94m"
    local font_bold="\033[1m"
    local font_end="\033[0m"

    echo -e "\n${font_blue}${font_bold}${1}${font_end}"
}

mkdir -p /os/images

echo_blue "[Create disk image]"
dd if=/dev/zero of=/os/images/${IMAGE_NAME}.img bs=$(expr 1024 \* 1024 \* 1024) count=${IMAGE_ORIGINAL_SIZE}

echo_blue "[Make partition]"
sfdisk /os/images/${IMAGE_NAME}.img < /os/assets/partition.script

echo_blue "\n[Format partition with ext4]"
losetup -D
LOOPDEVICE=$(losetup -f)
echo -e "\n[Using ${LOOPDEVICE} loop device]"
losetup -o $(expr 512 \* 2048) ${LOOPDEVICE} /os/images/${IMAGE_NAME}.img
mkfs.ext4 ${LOOPDEVICE}

echo_blue "[Copy ${IMAGE_NAME} directory structure to partition]"
mkdir -p /os/mnt
mount -t auto ${LOOPDEVICE} /os/mnt/
cp -R /os/rootfs/. /os/mnt/

echo_blue "[Setup extlinux]"
extlinux --install /os/mnt/boot/
cp /os/assets/syslinux.cfg /os/mnt/boot/syslinux.cfg

echo_blue "[Unmount]"
umount /os/mnt
losetup -D

echo_blue "[Write syslinux MBR]"
dd if=/usr/lib/syslinux/mbr/mbr.bin of=/os/images/${IMAGE_NAME}.img bs=440 count=1 conv=notrunc

echo_blue "[Create qcow2 image]"
qemu-img convert -c /os/images/${IMAGE_NAME}.img -O qcow2 /os/images/${IMAGE_NAME}.qcow2

echo_blue "[Expand qcow2 to ${IMAGE_DEFINITIVE_SIZE}]"
qemu-img resize /os/images/dandelion-worker-x86_64.qcow2 ${IMAGE_DEFINITIVE_SIZE}
cp -a /os/images/dandelion-worker-x86_64.qcow2 /os/images/dandelion-worker-x86_64-${IMAGE_DEFINITIVE_SIZE}.qcow2
virt-resize --expand /dev/sda1 /os/images/dandelion-worker-x86_64.qcow2 /os/images/dandelion-worker-x86_64-${IMAGE_DEFINITIVE_SIZE}.qcow2
mv /os/images/dandelion-worker-x86_64-${IMAGE_DEFINITIVE_SIZE}.qcow2 /os/images/dandelion-worker-x86_64.qcow2

echo_blue "[Create VirtualBox VDI image]"
qemu-img convert -O vdi /os/images/dandelion-worker-x86_64.qcow2 /os/images/dandelion-worker-x86_64.vdi

chmod 666 /os/images/*
