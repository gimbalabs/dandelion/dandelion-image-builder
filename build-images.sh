#!/bin/bash

export KUBECONFIG=kubeconfig
export DANDELION_BRANCH=update/v12.0.0
export DANDELION_REPO=https://gitlab.com/gimbalabs/dandelion/kustomize-dandelion.git

# cleanup any previous build
rm -rf rootfs dandelion-worker* images && mkdir rootfs images
footloose delete

footloose create

while ! `echo > /dev/tcp/127.0.0.1/2222`
do
  sleep 1
done
sleep 10
k3sup install --ip 127.0.0.1 --ssh-port 2222 --ssh-key cluster-key --user root --k3s-version v1.22.4+k3s1

rm -rf /tmp/dandelion.git
git clone --single-branch --branch ${DANDELION_BRANCH} ${DANDELION_REPO} /tmp/dandelion.git
for network in testnet mainnet
do
  kustomize build \
    --enable-helm \
    /tmp/dandelion.git/overlays/${network}-full > /tmp/dandelion.git/overlays/${network}-full/output.yaml
  kubectl get ns dandelion-${network} || kubectl create ns dandelion-${network}
  kubectl apply --validate=false -n dandelion-${network} -f /tmp/dandelion.git/overlays/${network}-full/output.yaml
done

footloose stop

docker export \
  -o dandelion-worker.tar \
  cluster-dandelion-worker-0
tar -xf dandelion-worker.tar -C rootfs

# workaround resolv.conf
rm rootfs/etc/resolv.conf && ln -s /run/systemd/resolve/resolv.conf rootfs/etc/resolv.conf

# IMAGE_ORIGINAL_SIZE is for the .img raw image
# IMAGE_DEFINITIVE_SIZE is for the .qcow2 virtual disk size
docker run -it \
   -v ${PWD}:/os:rw \
   -e IMAGE_NAME=dandelion-worker-x86_64 \
   -e IMAGE_ORIGINAL_SIZE=2 \
   -e IMAGE_DEFINITIVE_SIZE=200G \
   --privileged \
   --cap-add SYS_ADMIN \
   gimbalabs/dandelion-image-builder:builder bash -x /os/assets/bin/create-image.sh
